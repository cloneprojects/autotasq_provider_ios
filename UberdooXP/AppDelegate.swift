//
//  AppDelegate.swift
//  SwyftXP
//
//  Created by Karthik Sakthivel on 23/10/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import Alamofire
import SocketIO

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,MessagingDelegate,CLLocationManagerDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var locationManager : CLLocationManager!
    var manager : SocketManager!
    var socket : SocketIOClient!
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        GMSServices.provideAPIKey(Constants.mapsKey)
        GMSPlacesClient.provideAPIKey(Constants.placesKey)
        IQKeyboardManager.sharedManager().enable = true
        
        
        Messaging.messaging().delegate = self
        
        FirebaseApp.configure()
        Messaging.messaging().shouldEstablishDirectChannel = true

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        MainViewController.reloadPage = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Refresh"), object: self)
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        MainViewController.reloadPage = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Refresh"), object: self)
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        if(socket != nil){
            socket.disconnect()
        }
        if(locationManager != nil)
        {
            locationManager.stopUpdatingLocation()
        }
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn")
        manager = SocketManager(socketURL: URL(string: Constants.socketURL)!, config: [.log(true), .compress, .forcePolling(true)])
        socket = manager.defaultSocket
        if(socket != nil){
            socket.connect()
        }
        if(isLoggedIn)
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
            locationManager.requestWhenInUseAuthorization()
            locationManager.requestLocation()
            locationManager.startUpdatingLocation()
        }

    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationArray = locations as NSArray
        let locationObj = locationArray.lastObject as! CLLocation
        let coord = locationObj.coordinate
        let currentLatitude = String(coord.latitude)
        let currentLongitude = String(coord.longitude)
        let bearing = String(locationObj.course)
        
        let oldLatitude = UserDefaults.standard.double(forKey: "lastKnownLatitude")
        let oldLongitude = UserDefaults.standard.double(forKey: "lastKnownLongitude")
        
        let provider_id = UserDefaults.standard.string(forKey: "provider_id")
        
        let oldLocation : CLLocation = CLLocation.init(latitude: oldLatitude, longitude: oldLongitude)
        
        let meters: CLLocationDistance = locationObj.distance(from: oldLocation)
        print(meters)
//        if(meters > 10){
            let isLoggedIn = UserDefaults.standard.bool(forKey: "isLoggedIn")
            if(isLoggedIn)
            {
                socket.emit("UpdateLocation", ["latitude": currentLatitude,"longitude":currentLongitude,"provider_id":provider_id,"bearing":bearing])
//            self.updateLocation(latitude: currentLatitude,longitude: currentLongitude, bearing: bearing)
            }
            else{
                
            }
            UserDefaults.standard.set(coord.latitude, forKey: "lastKnownLatitude")
            UserDefaults.standard.set(coord.longitude, forKey: "lastKnownLongitude")

//        }
    }
    
    func updateLocation(latitude:String,longitude:String,bearing:String){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        print(latitude)
        print(longitude)
        let params: Parameters = [
            "latitude":latitude,
            "longitude": longitude,
            "bearing" : bearing
        ]
        
        
        let url = "\(Constants.baseURL)/update_location"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
           print(response)
        }

    }
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        locationManager.stopUpdatingLocation()
        if (error != nil)
        {
            print(error)
        }
    }
    
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        MainViewController.reloadPage = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Refresh"), object: self)
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        MainViewController.reloadPage = true
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Refresh"), object: self)
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
}

