//
//  FAQViewController.swift
//  Aclena Provider
//
//  Created by Pyramidions on 07/04/18.
//  Copyright © 2018 Uberdoo. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController {

    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    
    var titleVal = ""
    var message = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.titleLbl.text =  titleVal
        self.msgLbl.text = message

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func back(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
