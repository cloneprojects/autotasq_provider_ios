//
//  BookingTableViewCell.swift
//  SwyftXP
//
//  Created by Karthik Sakthivel on 01/11/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit

class BookingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var serviceDate: UILabel!
    @IBOutlet weak var subCategoryName: UILabel!
    @IBOutlet weak var providerImage: UIImageView!
    

    @IBOutlet weak var changeStatusLbl: UILabel!
    @IBOutlet weak var changeStatusButton: UIButton!
    
//    @IBOutlet weak var startToPlace: UIButton!
//    @IBOutlet weak var startJob: UIButton!
    
    @IBOutlet weak var serviceTime: UILabel!
    @IBOutlet weak var cornerView: UIView!
    
    @IBOutlet weak var providerName: UILabel!
    
    @IBOutlet weak var bookingStatusImageView: UIImageView!
    @IBOutlet weak var bookingStatusLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

