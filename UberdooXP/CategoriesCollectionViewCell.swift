//
//  CategoriesCollectionViewCell.swift
//  SwyftXP
//
//  Created by Karthik Sakthivel on 05/11/17.
//  Copyright © 2017 Swyft. All rights reserved.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var exprerience: UILabel!
    @IBOutlet weak var pricePerHour: UILabel!
    @IBOutlet weak var quickPitch: UILabel!
    @IBOutlet weak var editButton: UIButton!
    
}
